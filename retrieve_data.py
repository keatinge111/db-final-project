import wget
import zipfile
import os
from os import mkdir
from shutil import rmtree

with open('code/datasets.txt') as datasets_file:
    urls = datasets_file.readlines()

    if len(urls) > 5:
        raise Exception("Too many datasets were specified")

    rmtree('code/datasets', ignore_errors=True)
    mkdir('code/datasets')

    for u in urls:
        f = wget.download(u.rstrip(), 'code/datasets')
        print(f)

    with zipfile.ZipFile("code/datasets/PEP_2018_PEPAGESEX.zip", "r") as zip_ref:
        zip_ref.extractall("code/datasets/")

    for file in os.listdir("code/datasets/"):
        if not file.endswith(".csv"):
            os.remove(os.path.join("code/datasets", file))
