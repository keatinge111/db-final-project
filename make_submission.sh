rm -f db-final-project.zip
cd ..
zip db-final-project.zip db-final-project/code/*.{py,txt,sql} db-final-project/images/*.png db-final-project/{db-setup.sql,instructions.md,requirements.txt,retrieve_data.py,setup_db.sh}
mv db-final-project.zip db-final-project/db-final-project.zip