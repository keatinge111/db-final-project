DROP TABLE IF EXISTS daily_reports CASCADE;
DROP TABLE IF EXISTS counties CASCADE;
DROP TABLE IF EXISTS estimates CASCADE;
DROP TABLE IF EXISTS populations CASCADE;


CREATE TABLE counties
(
    fips  INT         NOT NULL PRIMARY KEY,
    name  VARCHAR(63) NOT NULL,
    state VARCHAR(31) NOT NULL,
    UNIQUE(name, state)
);

CREATE TABLE daily_reports
(
    date   DATE NOT NULL,
    fips   INT  NOT NULL REFERENCES counties, -- A global code for each county
    cases  INT  NOT NULL,
    deaths INT  NOT NULL,
    PRIMARY KEY (fips, date)
);
CREATE INDEX daily_reports_fips_index ON daily_reports using hash(fips);


CREATE TABLE estimates
(
    id       SERIAL NOT NULL PRIMARY KEY,
    date     DATE   NOT NULL,
    low_age  INT    NOT NULL,
    high_age INT    NOT NULL
);

CREATE TABLE populations
(
    estimate_id INT NOT NULL REFERENCES estimates(id),
    fips        INT NOT NULL REFERENCES counties,
    males       INT NOT NULL,
    females     INT NOT NULL,
    PRIMARY KEY (fips, estimate_id)
);
CREATE INDEX populations_fips_index ON populations using hash(fips);

