import psycopg2
import psycopg2.extras


def pct_wrap(str_to_wrap):
    return "%{}%".format(str_to_wrap)


def fetch_as_dicts(cursor):
    cols = [col.name for col in cursor.description]
    to_return = []
    for row in cursor.fetchall():
        to_return.append(dict(zip(cols, row)))
    return to_return


def fetch_with_headers(cursor):
    cols = [col.name for col in cursor.description]
    return [cols, cursor.fetchall()]


class DB:
    def __init__(self):
        self.conn = psycopg2.connect(
            user="covid_user", password="test_password", host="localhost", port="5432", database="covid_db",
        )

    def set_up_tables(self):
        with self.conn.cursor() as cursor:
            with open("schema.sql", "r") as covid_schema:
                setup_queries = covid_schema.read()
                cursor.execute(setup_queries)
            self.conn.commit()

    def insert_daily_report(self, date, fips, cases, deaths):
        cur = self.conn.cursor()
        query = "INSERT INTO daily_reports(date, fips, cases, deaths) VALUES (%s, %s, %s, %s)"
        cur.execute(query, (date, fips, cases, deaths))

    def insert_estimate(self, date, age_start, age_stop):
        cur = self.conn.cursor()
        query = "INSERT INTO estimates(date, low_age, high_age) VALUES (%s, %s, %s) RETURNING id"
        cur.execute(query, (date, int(age_start), int(age_stop)))
        db_id = cur.fetchone()[0]
        return db_id

    def commit(self):
        self.conn.commit()

    def insert_county(self, fips, name, state):
        cur = self.conn.cursor()
        query = "INSERT INTO counties(fips, name, state) VALUES (%s, %s, %s)"
        cur.execute(query, (fips, name, state))

    def insert_population(self, est_id, fips, males, females):
        cur = self.conn.cursor()
        query = "INSERT INTO populations(estimate_id, fips, males, females) VALUES (%s, %s, %s, %s)"
        cur.execute(query, (est_id, fips, males, females))

    def get_united_states_pop(self):
        """Gets most recent estimate of the united states population"""
        query = """
        SELECT SUM(males) + SUM(females)
        FROM estimates e
            INNER JOIN populations p
            ON e.id = p.estimate_id
        WHERE date = 
        (SELECT MAX(date) FROM estimates)
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query)
        return cur.fetchall()[0][0]

    def get_county_majority_breakdown_pop(self):
        """Gets the population of majority male and majority female counties"""
        query = """
        WITH cte AS (
            SELECT fips,
                   CASE WHEN (CAST(SUM(p.males) AS FLOAT) / SUM(p.females)) > 1 THEN 1 ELSE 0 END AS isMajorityMale,
                   SUM(p.males) + SUM(p.females)                                                  AS countyPopulation
            FROM estimates e
                     INNER JOIN populations p
                                ON e.id = p.estimate_id
            WHERE date =
                  (SELECT MAX(date) FROM estimates)
            GROUP BY fips
        )
        SELECT isMajorityMale, SUM(countyPopulation) AS countyPopulation
        FROM cte
        GROUP BY isMajorityMale
        ORDER BY isMajorityMale ASC
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query)
        return cur.fetchall()

    def get_deaths_by_county_majority(self, is_majority_male):
        """Gets total deaths each day for either majority male or majority female counties"""
        query = """
        WITH cte AS (
            SELECT fips,
                   FLOOR(SUM(p.males) / SUM(p.females)) AS isMajorityMale,
                   SUM(p.males) + SUM(p.females)        AS countyPopulation
            FROM estimates e
                     INNER JOIN populations p
                                ON e.id = p.estimate_id
            WHERE date =
                  (SELECT MAX(date) FROM estimates)
            GROUP BY fips
        )
        SELECT d.date, SUM(deaths) AS deaths
        FROM daily_reports d
                 INNER JOIN cte c
                            ON d.fips = c.fips
        WHERE c.isMajorityMale = %s
        GROUP BY d.date, c.isMajorityMale
        ORDER BY d.date ASC
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query, (is_majority_male,))
        return cur.fetchall()

    def states_like(self, state):
        query = """
        SELECT DISTINCT state
        FROM counties
        WHERE state ILIKE %s
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query, (state,))
        return cur.fetchall()

    def get_states_query(self):
        query = """
        SELECT DISTINCT state
        FROM counties
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query)
        return cur

    def get_states(self):
        cur = self.get_states_query()
        return fetch_with_headers(cur)

    def get_states_dict(self):
        cur = self.get_states_query()
        return fetch_as_dicts(cur)

    def get_all_counties_dict(self):
        query = "SELECT * from counties"
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query)
        return fetch_as_dicts(cur)

    def get_all_state_names(self):
        return [s["state"] for s in self.get_states_dict()]

    def counties_like(self, state, county):
        query = """
        SELECT name
        FROM counties
        WHERE state ILIKE %s
        AND name ILIKE %s
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query, (pct_wrap(state), pct_wrap(county)))
        return cur.fetchall()

    def get_counties(self, state):
        query = """
        SELECT DISTINCT name
        FROM counties
        WHERE state ILIKE %s
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query, (pct_wrap(state),))
        return fetch_with_headers(cur)

    def get_county_names_in_state(self, state):
        counties = self.get_counties_in_state(state)
        return [c["name"] for c in counties]

    def get_counties_in_state(self, state):
        query = """
        SELECT * FROM counties WHERE state = %s
        """
        cur = self.conn.cursor()
        cur.execute(query, (state,))
        return fetch_as_dicts(cur)

    def county_changes(self, state, county):
        """Get new daily deaths and new daily cases for a county"""
        query = """
        WITH cte AS (
            SELECT d.*, c.name,
            LAG(cases, 1) OVER (ORDER BY d.fips DESC, date ASC) prevCases, 
            LAG(deaths, 1) OVER (ORDER BY d.fips DESC, date ASC) prevDeaths
            FROM daily_reports d
                INNER JOIN counties c
            ON d.fips = c.fips
            WHERE c.name = %s
            AND c.state = %s
        ) 
        SELECT date, name AS county, cases, deaths, (cases - prevCases) AS newCases, (deaths - prevDeaths) AS newDeaths
        FROM cte
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query, (county, state))
        return fetch_with_headers(cur)

    def total_country_changes(self):
        """Get new daily deaths and new daily cases for the entire US"""
        query = """
        WITH cte AS (
            SELECT date, cases, deaths, 
            LAG(cases, 1) OVER (ORDER BY date ASC) prevCases, 
            LAG(deaths, 1) OVER (ORDER BY date ASC) prevDeaths
            FROM (
                SELECT date, SUM(cases) AS cases, SUM(deaths) AS deaths
                FROM daily_reports
                GROUP BY date
            ) z
        ) 
        SELECT date, cases, deaths, (cases - prevCases) AS newCases, (deaths - prevDeaths) AS newDeaths
        FROM cte
        """
        cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(query)
        return fetch_with_headers(cur)

    def age_demographics_csf(self, state, age_thresh):
        """Correlate the proportion of county population over a certain age threshold
        with the death rate for every county in the state"""
        query = """
        WITH older_count_tbl AS (
            SELECT fips, SUM(males) + SUM(females) AS older_count
            FROM populations
                     JOIN estimates ON populations.estimate_id = estimates.id
            WHERE low_age >= %s
              AND date = (SELECT MAX(date) FROM estimates)
            GROUP BY fips
        ),
             csf_tbl as (
                 SELECT fips, cases, deaths, CAST(deaths AS REAL) / cases AS csf
                 FROM daily_reports
                 WHERE date = (SELECT MAX(date) FROM daily_reports)
             ),
             total_pop_tbl AS (
                 SELECT fips, SUM(males) + SUM(females) AS total_pop
                 FROM populations
                          JOIN estimates ON populations.estimate_id = estimates.id
                 WHERE date = (SELECT MAX(date) FROM estimates)
                 GROUP BY fips
             )
        SELECT counties.name,
               counties.state,
               older_count_tbl.fips,
               older_count_tbl.older_count,
               cases,
               deaths,
               csf,
               total_pop,
               CAST(older_count AS REAL) / total_pop AS older_pct
        FROM older_count_tbl
                 JOIN csf_tbl ON older_count_tbl.fips = csf_tbl.fips
                 JOIN counties ON counties.fips = older_count_tbl.fips
                 JOIN total_pop_tbl ON total_pop_tbl.fips = older_count_tbl.fips
        WHERE deaths > 50
          AND state = %s
        ORDER BY csf
        """
        cur = self.conn.cursor()
        cur.execute(query, (age_thresh, state))
        return fetch_as_dicts(cur)

    def adjusted_cumulative_totals_by_state(self, states):
        """"Get total number of cases every day for the given states, using the days
        since 100 cases instead of the date"""
        query = """
        WITH state_daily AS (
            SELECT date, state, SUM(cases) sum_cases, SUM(deaths) as sum_deaths
            FROM daily_reports
                     JOIN counties ON daily_reports.fips = counties.fips
            GROUP BY (date, state)
        ),
             hundred_date AS (
                 SELECT state, MIN(date) AS first_100_date FROM state_daily WHERE sum_cases >= 100 GROUP BY state
             )
        SELECT state_daily.date,
               state_daily.state,
               state_daily.sum_cases,
               state_daily.sum_deaths,
               hundred_date.state,
               state_daily.date - hundred_date.first_100_date as days_in
        FROM state_daily
                 JOIN hundred_date ON state_daily.state = hundred_date.state
        WHERE state_daily.date >= hundred_date.first_100_date AND state_daily.state IN %s
        ORDER BY state_daily.state, state_daily.date
        """
        cur = self.conn.cursor()
        cur.execute(query, (tuple(states),))
        return fetch_as_dicts(cur)

    def case_fatality_rate_by_state(self):
        """Get the case fatality rate for every state in the US"""
        query = """
        SELECT state, SUM(cases) AS cases, SUM(deaths) AS deaths, CAST(SUM(deaths) AS REAL) / SUM(cases) AS csf
        FROM daily_reports
                 JOIN counties ON daily_reports.fips = counties.fips
        WHERE date = (SELECT MAX(date) FROM daily_reports)
        GROUP BY state ORDER BY state;
        """
        cur = self.conn.cursor()
        cur.execute(query)
        return fetch_as_dicts(cur)

    def daily_csf_by_state(self, states):
        """Get the daily case fatality rate (new cases / new deaths) for each day for a state"""
        query = """
        WITH state_daily AS (
            SELECT date, state, SUM(cases) sum_cases, SUM(deaths) AS sum_deaths
            FROM daily_reports
                     JOIN counties ON daily_reports.fips = counties.fips
            GROUP BY (date, state)
        ),
             state_daily_change AS (
                 SELECT date,
                        state,
                        sum_cases,
                        sum_deaths,
                        sum_cases - LAG(sum_cases) OVER (PARTITION BY state ORDER BY date)   AS new_cases,
                        sum_deaths - LAG(sum_deaths) OVER (PARTITION BY state ORDER BY date) AS new_deaths
                 FROM state_daily
             )
        SELECT *, CAST(new_deaths AS REAL) / GREATEST(new_cases, 1) AS daily_csf
        FROM state_daily_change
        WHERE state IN %s AND new_cases IS NOT NULL
        ORDER BY (state, date);
        """
        cur = self.conn.cursor()
        cur.execute(query, (tuple(states),))
        return fetch_as_dicts(cur)

    def prop_infected_over_time_by_county(self, county_fips):
        """Get the proportion of county populations infected over time for multiple counties"""
        query = """
        WITH county_pops AS (
                 SELECT date, fips, SUM(males) + SUM(females) AS pop
                 FROM estimates
                          JOIN populations ON populations.estimate_id = estimates.id
                 WHERE date = (SELECT MAX(date) FROM estimates)
                 GROUP BY (date, fips)
             )
        SELECT daily_reports.date, state, name, cases, deaths, pop, CAST(cases AS REAL)/pop AS prop_cases FROM daily_reports
                          JOIN counties ON daily_reports.fips = counties.fips
                          JOIN county_pops ON daily_reports.fips = county_pops.fips
                          WHERE daily_reports.fips IN %s
        ORDER BY name, date
        """
        cur = self.conn.cursor()
        cur.execute(query, (tuple(county_fips),))
        return fetch_as_dicts(cur)
