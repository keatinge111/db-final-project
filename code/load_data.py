import os
import re
import csv
import time
import datetime

import database


def load_ny_times_covid_dataset(db, counties_csv):
    with open(counties_csv, newline="") as f:
        reader = csv.DictReader(f)

        for r in reader:
            if not r["fips"]:
                continue
            db.insert_daily_report(r["date"], r["fips"], r["cases"], r["deaths"])

    db.commit()
    print("> Finished loading NY Times covid-19 dataset")


def load_census(db, data_dir):
    metadata_path = os.path.join(data_dir, "PEP_2018_PEPAGESEX_metadata.csv")
    census_to_db_ids = {}
    print("> Loading estimates")
    with open(metadata_path, newline="") as f:
        reader = csv.reader(f)
        regex = re.compile(r"^est(\d{1,2})(\d{4})sex(\d)_age(.*)$")
        for census_id, census_desc in reader:
            match = re.match(regex, census_id)
            if not match:
                continue  # Not a population estimate

            month, year, sex, age = match.groups()
            month = int(month)
            year = int(year)
            sex = int(sex)

            if age == "85plus":
                age_start = 85
                age_stop = 150
            elif "to" in age:
                age_start, age_stop = map(int, age.split("to"))
            else:
                continue

            if sex != 0:
                continue  # Ignore duplicates for male/female/both
            if age_start == 0 and age_stop == 17:
                break  # Done with the regular 5 year age brackets, ignore the others

            date = datetime.datetime(year=year, month=month, day=1)
            db_id = db.insert_estimate(date, int(age_start), int(age_stop))
            census_to_db_ids[census_id] = db_id

    data_path = os.path.join(data_dir, "PEP_2018_PEPAGESEX_with_ann.csv")
    # See https://www.census.gov/programs-surveys/geography/technical-documentation/user-note/special-characters.html
    # for why this uses iso-8859-1
    with open(data_path, newline="", encoding="iso-8859-1") as f:
        reader = csv.DictReader(f)
        next(reader)  # skip first row (it's more labels)
        prev_state = None
        for row in reader:
            fips = row["GEO.id2"]
            county_and_state = row["GEO.display-label"]
            county, state = county_and_state.split(", ")
            if state != prev_state:
                print("> Loading population breakdown for", state)
                prev_state = state

            db.insert_county(fips, county, state)
            for census_est, db_est_id in census_to_db_ids.items():
                male_est = census_est.replace("sex0", "sex1")
                female_est = census_est.replace("sex0", "sex2")
                db.insert_population(db_est_id, fips, row[male_est], row[female_est])
    db.commit()
    print("> Finished loading Census dataset")


def setup(db):
    t1 = time.time()
    print("Loading US Census population estimates")
    load_census(db, "./datasets")
    print("\nLoading NY times covid-19 dataset")
    load_ny_times_covid_dataset(db, "./datasets/us-counties.csv")
    print("\nSuccessfully loaded both datasets in {:.2f} seconds".format(time.time() - t1))


def main():
    db = database.DB()
    db.set_up_tables()
    setup(db)


if __name__ == "__main__":
    main()
