import sys
import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rcParams
from tabulate import tabulate
import fuzzywuzzy.process
import fuzzywuzzy.fuzz

import database


def table_from_list_of_dicts(list_of_dicts, col_mapping):
    real_names, display_names = list(zip(*col_mapping.items()))
    table_rows = [[el[k] for k in real_names] for el in list_of_dicts]
    return tabulate(table_rows, headers=display_names)


def correlate_age_csf(db, state, age_thresh):
    csf_by_age = db.age_demographics_csf(state, age_thresh)
    print(
        table_from_list_of_dicts(
            csf_by_age,
            {
                "state": "State",
                "name": "County",
                "csf": "Case fatality rate",
                "older_pct": "Prop. age {}+".format(age_thresh),
                "cases": "Cases",
                "deaths": "Deaths",
            },
        )
    )

    older_pct = [r["older_pct"] for r in csf_by_age]
    csf = [r["csf"] for r in csf_by_age]
    plt.scatter(x=older_pct, y=csf)
    plt.xlabel("Proportion of population age {}+".format(age_thresh))
    plt.ylabel("Case fatality rate (deaths/cases)")
    plt.title("Proportion of older population vs case fatality rate in {}".format(state))

    if len(older_pct) > 2:
        # From https://stackoverflow.com/questions/22239691/code-for-best-fit-straight-line-of-a-scatter-plot-in-python
        plt.plot(
            np.unique(older_pct), np.poly1d(np.polyfit(older_pct, csf, 1))(np.unique(older_pct)), color="green",
        )

    plt.show()


def view_death_rates(db):
    total_death_rates = db.total_country_changes()[1]
    dates = [row[0] for row in total_death_rates]
    total_us_census_count = int(db.get_united_states_pop())
    total_deaths_pct = [float(row[2]) / total_us_census_count for row in total_death_rates]

    mf_county_populations = db.get_county_majority_breakdown_pop()

    female_majority_county_population = int(mf_county_populations[0][1])
    male_majority_county_population = int(mf_county_populations[1][1])

    female_majority_county = db.get_deaths_by_county_majority(0)
    male_majority_county = db.get_deaths_by_county_majority(1)

    female_dates = [row[0] for row in female_majority_county]
    female_majority_county_deaths = [
        float(row[1]) / female_majority_county_population for row in female_majority_county
    ]
    male_dates = [row[0] for row in male_majority_county]
    male_majority_county_deaths = [float(row[1]) / male_majority_county_population for row in male_majority_county]

    plt.plot(dates, total_deaths_pct, c="g", label="Entire United States")
    plt.plot(
        female_dates, female_majority_county_deaths, c="r", label="majority female counties",
    )
    plt.plot(male_dates, male_majority_county_deaths, c="b", label="majority male counties")
    plt.xlabel("Date")
    plt.ylabel("Percent of population infected")
    plt.title("Deaths in the United States broken down by county gender majority")
    plt.xticks(rotation=45)
    plt.legend()
    plt.show()


def print_table(table_list):
    cols, table = table_list
    print(tabulate(table, headers=cols))


def get_int():
    int_inp = input()
    try:
        return int(int_inp)
    except ValueError:
        print("Invalid input, try again")
        return get_int()


def age_csf_command(db):
    print("What state would you like to look at? (For example: New York)")
    states = [e["state"] for e in db.get_states_dict()]
    best_state = choose_one_fuzzy(states)
    print("What age threshold would you like to use for counting the older population? (For example: 65)")
    age_thresh = get_int()
    correlate_age_csf(db, best_state, age_thresh)


def choose_one(choices, display_fn=lambda x: x):
    for i, choice in enumerate(choices):
        print("{}. {}".format(i, display_fn(choice)))

    choice_num = input()
    try:
        return choices[int(choice_num)]
    except (IndexError, ValueError):
        print("Invalid input, try again")
        return choose_one(choices, display_fn)


def choose_one_fuzzy(choices):
    query = input()
    best, score = fuzzywuzzy.process.extractOne(query, choices, scorer=fuzzywuzzy.fuzz.token_sort_ratio)
    if score == 0:
        print("No good matches, try again")
        choose_one_fuzzy(choices)
    return best


def fuzzy_match_by_key(query, choices, key_fn):
    transformed = [key_fn(c) for c in choices]
    best, _ = fuzzywuzzy.process.extractOne(query, transformed, scorer=fuzzywuzzy.fuzz.token_set_ratio)
    for choice in choices:
        if key_fn(choice) == best:
            return choice


def parse_states_list(db, states_str):
    raw_states_list = states_str.strip().replace(", ", ",").split(",")

    actual_states = db.get_all_state_names()
    correct_states = [fuzzywuzzy.process.extractOne(inp, actual_states)[0] for inp in raw_states_list]
    return correct_states


def adj_cumulative_states_command(db):
    print("What states (separate with ',') would you like to look at? (For example 'New York, New Jersey, Vermont')")
    states = parse_states_list(db, input())
    results = db.adjusted_cumulative_totals_by_state(states)
    print(
        table_from_list_of_dicts(
            results,
            {
                "date": "Date",
                "state": "State",
                "sum_cases": "Cumulative Cases",
                "sum_deaths": "Cumulative Deaths",
                "days_in": "Days since 100 cases",
            },
        )
    )
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for state, group in itertools.groupby(results, key=lambda x: x["state"]):
        x_vals = []
        y_vals = []
        for el in group:
            x_vals.append(el["days_in"])
            y_vals.append(el["sum_cases"])
        ax.plot(x_vals, y_vals, label=state)
    plt.xlabel("Days since 100 cases")
    plt.ylabel("Cumulative cases")
    plt.legend()
    plt.show()


def csf_by_state_command(db):
    state_csf = db.case_fatality_rate_by_state()
    print(
        table_from_list_of_dicts(
            state_csf, {"state": "State", "cases": "Cases", "deaths": "Deaths", "csf": "Case fatality rate",},
        )
    )

    ys = [r["csf"] for r in state_csf]
    states = [r["state"] for r in state_csf]
    plt.yticks(np.arange(len(state_csf)), labels=states, rotation=45)
    plt.barh(np.arange(len(state_csf)), ys)
    plt.margins(x=0.01, y=0.01)
    plt.ylabel("Case fatality rate")
    plt.show()


def weekly_xticks(all_dates):
    dates_df = pd.Series(all_dates)
    date_range = pd.date_range(dates_df.min(), dates_df.max(), freq="W")
    return date_range


def daily_csf_by_state(db):
    print("Which states would you like to look at? (For example 'New York, New Jersey, Connecticut')")
    states = parse_states_list(db, input())
    csf_by_state_results = db.daily_csf_by_state(states)
    print(
        table_from_list_of_dicts(
            csf_by_state_results,
            {
                "state": "State",
                "date": "Date",
                "new_cases": "New Cases",
                "new_deaths": "New deaths",
                "daily_csf": "Daily case fatality rate",
            },
        )
    )

    all_dates = []
    for state, group in itertools.groupby(csf_by_state_results, key=lambda x: x["state"]):
        state_xs = []
        state_ys = []
        for day in group:
            state_xs.append(day["date"])
            state_ys.append(day["daily_csf"])
        all_dates.extend(state_xs)
        plt.plot(state_xs, state_ys, label=state)

    plt.legend()
    plt.xticks(weekly_xticks(all_dates), rotation=45)
    plt.ylabel("Daily case fatality rate")
    plt.show()


def find_county_command(db):
    print("Which state?")
    actual_states = db.get_all_state_names()
    correct_state = choose_one_fuzzy(actual_states)
    _, counties = db.get_counties(correct_state)
    print(tabulate(counties, headers=["Counties in {}".format(correct_state)]))


def best_county_match(county_dicts, test_str):
    return fuzzy_match_by_key(test_str, county_dicts, key_fn=lambda x: "{} {}".format(x["name"], x["state"]))


def county_changes_command(db):
    print("Which county? (For example 'Albany New York')")
    correct_county = best_county_match(db.get_all_counties_dict(), input())
    print("County changes since date of first case in {}, {}".format(correct_county["name"], correct_county["state"]))
    headers, rows = db.county_changes(correct_county["state"], correct_county["name"])
    print_table((headers, rows))

    dates_list = []
    cases_list = []
    deaths_list = []

    for date, county, cases, deaths, new_cases, new_deaths in rows:
        dates_list.append(date)
        cases_list.append(cases)
        deaths_list.append(deaths)

    plt.plot(dates_list, cases_list, label="Cases")
    plt.plot(dates_list, deaths_list, label="Deaths")
    plt.legend()
    plt.title("{}, {}".format(correct_county["name"], correct_county["state"]))
    plt.xticks(weekly_xticks(dates_list), rotation=45)
    plt.show()


def get_counties_input(db):
    print(
        "Enter a list of counties separated by commas. (For example"
        " 'Rensselear New York, Albany New York, Saratoga New York')"
    )
    county_state_groups = [g.strip() for g in input().split(",") if g.strip()]
    county_dicts = db.get_all_counties_dict()
    chosen_county_dicts = [best_county_match(county_dicts, g) for g in county_state_groups]
    county_names = ", ".join("{} ({})".format(x["name"], x["state"]) for x in chosen_county_dicts)
    print("Looking at", county_names)
    return chosen_county_dicts


def county_prop_command(db):
    counties = get_counties_input(db)
    fips = [c["fips"] for c in counties]

    results = db.prop_infected_over_time_by_county(fips)
    print(
        table_from_list_of_dicts(
            results,
            {
                "state": "State",
                "name": "County",
                "date": "Date",
                "cases": "Cases",
                "deaths": "Deaths",
                "pop": "Population",
                "prop_cases": "Proportion infected",
            },
        )
    )

    all_dates = []
    for county, group in itertools.groupby(results, key=lambda x: x["name"]):
        x_vals = []
        y_vals = []
        full_name = None
        for day in group:
            if full_name is None:
                full_name = "{}, {}".format(day["name"], day["state"])
            x_vals.append(day["date"])
            y_vals.append(day["prop_cases"])
        all_dates.extend(x_vals)
        plt.plot(x_vals, y_vals, label=full_name)

    plt.xticks(weekly_xticks(all_dates), rotation=45)
    plt.ylabel("Cases / Population")
    plt.legend()
    plt.show()


def credits_command(_):
    print("COVID-19 data by NY Times: https://github.com/nytimes/covid-19-data")
    print(
        "Population estimates by the US census: "
        "https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-detail.html"
    )


def exit_command(_):
    sys.exit()


def main():
    rcParams.update({"figure.autolayout": True})
    db = database.DB()
    commands = [
        ("Exit", exit_command),
        ("Explore the effect of age demographics on the case fatality rate for a state", age_csf_command,),
        (
            "Compare cumulative Covid-19 cases among different states adjusted by days since 100 cases",
            adj_cumulative_states_command,
        ),
        ("Compare the case fatality rates among the states", csf_by_state_command),
        ("Compare the daily case fatality rate over time between states", daily_csf_by_state,),
        ("Search for a county", find_county_command),
        (
            "Compare the proportion of the population that has tested positive within different counties over time",
            county_prop_command,
        ),
        ("View the cases and deaths over time for a county", county_changes_command),
        ("View deaths in the United States broken down by county gender majority", view_death_rates,),
        ("Credits", credits_command),
    ]

    while True:
        print("What would you like to do? Enter the number next to the command you want to run")
        choice, fn = choose_one(commands, display_fn=lambda x: x[0])
        fn(db)
        input("Press enter to continue")
        print()


if __name__ == "__main__":
    main()
