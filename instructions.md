# Database Systems Final Project

## Rendered markdown
You can see this file rendered nicely in Gitlab at https://gitlab.com/keatinge111/db-final-project/-/blob/master/instructions.md

## Video
You can find our video here: https://www.youtube.com/watch?v=pTttkxov7-Y

## Installation
Note: We recommend NOT running in docker. Our project makes heavy use of matplotlib, the query outputs
may be difficult to interpret without seeing the plots. If you must use docker, the following stack overflow post
may be helpful in getting the plots to display https://stackoverflow.com/questions/46018102/how-can-i-use-matplotlib-pyplot-in-a-docker-container

1. Go to the `db-final-project` directory
1. Install the dependencies to download the datasets with `pip3 install -r requirements.txt`
1. Download the datasets with `python3 retrieve_data.py`
1. Run `chmod +x setup_db.sh` to prepare to run the db setup script
1. Create the database and user by running `sudo ./setup_db.sh`
1. Go to the `db-final-project/code` directory
1. Install dependencies with `pip3 install -r requirements.txt`
1. Load the datasets into the database with `python3 load_data.py` (this may take a few minutes, the Census dataset is large)
1. The installation is now complete, start the application with `python3 application.py`
1. The initial prompt will ask you to select a command, enter a number (0-9) from the list of commands.
1. Please see the documentation below on the specifics of all the commands and some suggested inputs.

## Sources
1. COVID-19 data by The New York Times: [https://github.com/nytimes/covid-19-data](https://github.com/nytimes/covid-19-data)
1. Population estimates by the US census: [https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-detail.html](https://www.census.gov/data/tables/time-series/demo/popest/2010s-counties-detail.html)


## Commands
Note: Commands for entering state/county will accept any input, then use fuzzy string matching to determine the nearest state/county.
For example, after selecting command 7, the user can type `Wayn Mich` which will automatically be corrected to `Wayne County Michigan`

### Explore the effect of age demographics on the case fatality rate for a state
This command is used to look for a correlation between the amount of older folks in a county
and the case fatality rate (cases/deaths) in that county for all counties in a state. 

Inputs:

1. The state that you would like to analyze counties within.  For example, "New York"
2. The age threshold to be used for counting the older population. If you enter 65, then
the correlation will be between the proportion of the county population that is age 65+
 and the case fatality rate within that county.

Interesting example inputs:

1. State = `Pennslyvania`, Age threshold = `75` (positive correlation with lots of samples)
1. State = `California`, Age threshold = `75` (weak negative correlation / no correlation)
1. State = `New York`, Age threshold = `65` (positive correlation)
1. State = `Georgia` Age threshold = `70` (very strong correlation, small sample size though)

Notes:

1. Only counties with at least 50 deaths are included in the analysis. If a county is missing,
it is because it does not have at-least 50 deaths. The data is case fatality rate is too noisy
if counties with less than 50 deaths are included.


Example output for State = `New York`, Age threshold = `65` (positive correlation)


![](images/Figure_1.png)


### Compare cumulative Covid-19 cases among different states adjusted by days since 100 cases
This command is used to compare the growth in the number of COVID-19 cases over time between 
different states. Instead of plotting the date and the number of cases at that date, the dates
are instead adjusted to be relative, the number of days since that state first hit 100 cases. 
This type of adjusted analysis makes it easier to compare states which had initial outbreaks
at different points in time.

Inputs:

1. List of states, for example `New York, New Jersey, Vermont`

Interesting example inputs

1. States = `Pennslyvania, New York, Massachusets, New Jersey`
1. States = `California, Texas, Washington` 
1. States = `Illinois, Michigan, Indiana, Ohio` 

Example output for states = `California, Texas, Washington`

![](images/Figure_2.png)

### Compare the case fatality rate among the states
This commands show the case fatality rate (cases / deaths) for each state in the US.

Example output:

![](images/Figure_3.png)


### Compare the daily case fatality rate over time between states
This command shows you a plot of the daily case fatality rate (new daily cases / new daily deaths) for each day for multiple states.

Inputs:

1. List of states, for example `Connecticut, New York, New Jersey`

Interesting example inputs:

1. States = `Connecticut, New York, New Jersey`
1. States = `California, Texas, Louisiana`
1. States = `Indiana, Ohio, Colorado`
1. States = `Florida, Georiga, Texas` (100% case fatality rates imply poor/non-existent testing)

![](images/Figure_4.png)


### Search for a county
This is command lists all counties in a given state.

Inputs:

1. A state name, for example `Connecticut`

Example output for `Connecticut`:

```
Counties in Connecticut
-------------------------
Fairfield County
Hartford County
Litchfield County
Middlesex County
New Haven County
New London County
Tolland County
Windham County
```


### Compare the proportion of the population that has tested positive within different counties over time
This command plots the ratio of the number of cases divided by the population for different counties over time.
This allows easier comparision between counties with very different population sizes.

Inputs:

1. A list of counties, for example `Rensselaer New York, Albany New York, Saratoga New York`


Interesting example inputs:

1. Counties = `Wayne Michigan, Nassau New York, Bergen New Jersey, Hudson New Jersey`
1. Counties = `Rensselaer New York, Albany New York, Saratoga New York`
1. Counties = `Nassau New York, Suffolk New York, Westchester New York`
1. Counties = `Essex New Jersey, Union New Jersey, Suffolk Massachusets, Miami Dade Florida,  Rockland New York, Middlesex New Jersey`

![](images/Figure_6.png)

### View the cases and deaths over time for a county
This command plots the number of cases and deaths for every day for a specific county

Inputs:

1. A single county, for example `Rensselear New York`


Interesting example inputs:

1. County = `Wayne Michigan`
1. County = `Nassau New York`
1. County = `Albany New York`
1. County = `Cook Illinois`
1. County = `Los Angelos California`

![](images/Figure_7.png)


### View deaths in the United States broken down by county gender majority
This command allows you to compare the number of deaths between counties that are majority-female and counties that are majority-male.

![](images/Figure_8.png)


